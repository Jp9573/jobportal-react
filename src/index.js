import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

class Card extends React.Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{this.props.title}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{this.props.companyName}</h6>
                    <p className="card-text mt-3"><strong>Location:</strong> {this.props.location} <br />
                        <strong>Experience:</strong> {this.props.experience} <br />
                        <strong>Skills:</strong> {this.props.skills} <br />
                        <strong>Job Description:</strong> {this.props.jd} <br />
                        <strong>Salary:</strong> {this.props.salary} <br />
                        <strong>Type:</strong> {this.props.type} <br />
                        <strong>Start date:</strong> {this.props.startdate} <br />
                        <strong>End date:</strong> {this.props.enddate} <br />
                        <strong>Source:</strong> {this.props.source} <br />
                    </p>
                    <a href={this.props.applylink} rel="noopener noreferrer" target="_blank" className="card-link">Apply now</a>
                </div>
            </div>
        )
    }
}

class FetchAPIDataComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            error: null,
            isLoaded: false,
            items: {},
            savedItems: {}
        };
        this.filterHandler = this.filterHandler.bind(this);
    }

    componentDidMount() {
        fetch("https://nut-case.s3.amazonaws.com/jobs.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result,
                        savedItems: result,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    _basicFilter(searcjQery, savedItems, attr) {
        let items = savedItems.data.filter((el) => {
            let searchValue = el[attr].toLowerCase();
            return searchValue.indexOf(searcjQery) !== -1;
        })
        return items;
    }

    _experienceFilter(searcjQery, savedItems) {
        let items = {};
        if (searcjQery === "Freshers") {
            let searcjQery = "fresher";
            items = savedItems.data.filter((el) => {
                let targetValue = el.experience.toLowerCase();
                if (targetValue === "0 yrs")
                    return true
                return targetValue.indexOf(searcjQery) !== -1;
            })
        } else if (searcjQery === "Experience") {
            items = savedItems.data;
        } else {
            let min = searcjQery.split(' ')[0], max = searcjQery.split(' ')[2];
            if (min === "10+") {
                min = 10;
                max = Infinity;
            }

            items = savedItems.data.filter((el) => {
                let targetValue = el.experience.toLowerCase().trim();
                if (targetValue !== undefined && !targetValue.startsWith("fresher") && targetValue !== "") {
                    let mini = targetValue.split(' ')[0].split('-')[0];
                    let maxi = targetValue.split(' ')[0].split('-')[1];
                    if (maxi === undefined) {
                        maxi = targetValue.split(' ')[2];
                    }
                    return (Number(min) <= Number(mini) && Number(mini) <= Number(max) && Number(maxi) <= Number(max));
                }
                return false;
            })
        }
        return items;
    }

    _activeTypeFilter(searcjQery, savedItems) {
        let today = new Date();
        let items = {};
        if (searcjQery === "Expired Jobs") {
            items = savedItems.data.filter((el) => {
                let dt = el.enddate;
                if (dt !== undefined && dt !== "") {
                    let dtObj = new Date(dt);
                    if (today > dtObj)
                        return true;
                }
                return false;
            })
        } else if (searcjQery === "Active Jobs") {
            items = savedItems.data.filter((el) => {
                let dt = el.startdate;
                if (dt !== undefined && dt !== "") {
                    let dtObj = new Date(dt);
                    if (today >= dtObj)
                        return true;
                }
                return false;
            })
        } else {
            items = savedItems.data;
        }
        return items;
    }

    filterHandler(action, event) {
        let items = {};

        if (['companyname', 'title', 'location', 'skills'].indexOf(action) >= 0) {
            items = this._basicFilter(event.target.value.toLowerCase().trim(), this.state.savedItems, action);
        } else if (action === 'experience') {
            items = this._experienceFilter(event.target.value, this.state.savedItems);
        } else if (action === 'active') {
            items = this._activeTypeFilter(event.target.value, this.state.savedItems);
        }

        this.setState({
            items: { 'data': items, 'len': items.length }
        })
    }

    render() {
        const { error, isLoaded, items } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="loader"></div>;
        } else {
            const count = items.len;
            return (
                <div className="holder">
                    <div className="container-fluid bg-light">
                        <div className="row align-items-center justify-content-center">
                            <div className="col-md-3 pt-3">
                                <div className="form-group ">
                                    <div className="input-group input-group-lg">
                                        <input type="text" className="form-control" style={{ height: "38px" }} placeholder="Company" onChange={this.filterHandler.bind(this, 'companyname')} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 pt-3">
                                <div className="form-group ">
                                    <div className="input-group input-group-lg">
                                        <input type="text" className="form-control" style={{ height: "38px" }} placeholder="Position" onChange={this.filterHandler.bind(this, 'title')} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 pt-3">
                                <div className="form-group ">
                                    <div className="input-group input-group-lg">
                                        <input type="text" className="form-control" style={{ height: "38px" }} placeholder="Location" onChange={this.filterHandler.bind(this, 'location')} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-1 pt-4">
                                <h4>Jobs</h4>
                            </div>
                        </div>
                        <div className="row align-items-center justify-content-center">
                            <div className="col-md-3">
                                <div className="form-group ">
                                    <div className="input-group input-group-lg">
                                        <input type="text" className="form-control" style={{ height: "38px" }} placeholder="Skill" onChange={this.filterHandler.bind(this, 'skills')} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <select id="inputState" className="form-control" onChange={this.filterHandler.bind(this, 'experience')}>
                                        <option selected>Experience</option>
                                        <option>Freshers</option>
                                        <option>1 - 3 Years</option>
                                        <option>4 - 7 Years</option>
                                        <option>8 - 10 Years</option>
                                        <option>10+ Years</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group ">
                                    <select id="inputState" className="form-control" onChange={this.filterHandler.bind(this, 'active')}>
                                        <option selected>All Jobs</option>
                                        <option>Active Jobs</option>
                                        <option>Expired Jobs</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-1 pb-4">
                                <h4><span className="badge badge-secondary">{count}</span></h4>
                            </div>
                        </div>
                    </div>
                    <div className="mainContent">
                        <div className="card-columns">
                            {items.data.map((item) => {
                                return <Card title={item.title}
                                    companyName={item.companyname}
                                    location={item.location}
                                    experience={item.experience}
                                    applylink={item.applylink}
                                    skills={item.skills}
                                    jd={item.jd}
                                    salary={item.salary}
                                    type={item.type}
                                    startdate={item.startdate}
                                    enddate={item.enddate}
                                    source={item.source}
                                />
                            })}
                        </div>
                    </div>
                </div >
            );
        }
    }
}

ReactDOM.render(
    <FetchAPIDataComponent />,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
